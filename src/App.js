import React, { useEffect, useState } from 'react';
import Book from './components/Book';

import { getToken, getProducts } from './api';

const App = (props) => {
  const [products, setProducts] = useState({});
  
  useEffect(() => {
    getToken().then(() => {
      getProducts(props.id).then((productsRes) => {
        // console.log('Products: ', products);
        setProducts(productsRes);
      });
    });
  }, []);
  
  return (
    <>
      <Book bookConfig={props} />
    </>

  );
};

export default App;
