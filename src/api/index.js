import axios from "axios";

import { checkLocalStorage, setLocalStorage, getLocalStorageKeyValue } from '../utils/localstorage';

const api = axios.create({
  baseURL: 'http://api.bds.mediacy.it/dogebds',
});

export const getToken = () => {
  return new Promise((resolve, reject) => {
    api.post(`/api/authorize/login`, {
      'username': 'admin',
      'password': 'hrQb94lm.q60FT!'
    }).then((res) => {
      const token = res.data.token.split(' ')[1];
      // console.log('Token:', token);
  
      if (!checkLocalStorage('token')) {
        setLocalStorage('token', token);
      }

      resolve();
  
    }).catch((err) => {
      console.log('Login Error: ', err);
      reject(err);
    });
  });
};

export const getProducts = (id, pageNum, pageSize, deleted) => {
  return new Promise((resolve, reject) => {
    api.get(`/api/product/listProduct?id=${id}&pageNum=1&pageSize=20&deleted=false`, {
      headers: {
        "accept-language": `"it"`,
        'Authorization': `Bearer ${getLocalStorageKeyValue('token', true)}`
      }
    }).then((res) => {
      resolve(res.data);
    }).catch((err) => {
      console.log('Login error: ', err);
      reject(err);
    });
  })
}