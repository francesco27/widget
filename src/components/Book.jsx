import React from 'react';

const Book = ({ bookConfig }) => {
  return (
    <span>
      {bookConfig.id} <br />
      {bookConfig.lang}
    </span>
  );
};

export default Book;
