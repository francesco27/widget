import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

const widgetDivs = document.querySelectorAll('.prenota-widget');

widgetDivs.forEach(div => {
  ReactDOM.render(
    <React.StrictMode>
      <App lang={div.dataset.lang} id={div.dataset.id} />
    </React.StrictMode>,
    div
  );
});

