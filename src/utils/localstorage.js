export const checkLocalStorage = (storageKeyName) => localStorage.getItem(storageKeyName) !== null ? true : false;

export const setLocalStorage = (storageKeyName, payload) => localStorage.setItem(storageKeyName, JSON.stringify(payload));

export const getLocalStorageKeyValue = (storageKeyName, parsed) => parsed ? JSON.parse(localStorage.getItem(storageKeyName)) : localStorage.getItem(storageKeyName);
